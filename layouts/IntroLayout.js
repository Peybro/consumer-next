import Head from "next/head";
import Consumer from "../components/Consumer";

export default function Introlayout({ children }) {
  return (
    <div className="intro-page">
      <Head>
        <title>Overview</title>
        <meta name="description" content="Do this or that." />
      </Head>
      <div className="intro-background">
        <Consumer amount={4000} />
      </div>

      <div className="intro-content">{children}</div>

      <style jsx>
        {`
          .intro-page {
            height: 100vh;
            width: 100vw;
            overflow: hidden;
            display: flex;
            justify-content: center;
            align-items: center;
            position: relative;
          }

          .intro-background {
            font-style: italic;
            position: absolute;
            word-break: break-all;
          }

          .intro-content {
            z-index: 10;
          }

          @media screen and (max-width: 320px) {
            .intro-page {
              overflow-y: scroll;
              align-items: initial;
            }

            .intro-content {
              margin-top: 20px;
            }
          }
        `}
      </style>
    </div>
  );
}
