import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import Head from "next/head";

import Layout from "../layouts/IntroLayout";
import Video from "./../components/Video";
import Back from "./../components/Back";

const videos = [
  {
    title: "Ephemeral Device",
    src: "https://www.youtube.com/embed/JmkXcFBG1OA",
  },
];

export default function Cinema() {
  const [showVideos, setShowVideos] = useState(false);
  const [rememberDecision, setRememberDecision] = useState(false);

  useEffect(() => {
    if (Cookies.get("showVideos") === "true") setShowVideos(true);
  }, []);

  function handleShowVideos() {
    if (rememberDecision) {
      Cookies.set("showVideos", "true", { expires: 30, path: "" });
    }
    setShowVideos(true);
  }

  return (
    <Layout>
      <div className="cinema-page">
        <Head>
          <title>Cinema</title>
          <meta name="description" content="Watch Movies. Enjoy." />
        </Head>
        <Back to="/intro" />
        {!showVideos && (
          <div className="cookie-info">
            <h1>
              Loading these videos will make YouTube safe cookies in your
              browser
            </h1>
            <div className="cookie-checkbox">
              <input
                type="checkbox"
                checked={rememberDecision}
                onChange={() => setRememberDecision(!rememberDecision)}
                id="rememberme"
              />
              <label htmlFor="rememberme">
                {" "}
                remember my decision (will set an additional cookie)
              </label>
            </div>
            <button className="okBtn" onClick={() => handleShowVideos()}>
              Ok load them
            </button>
          </div>
        )}
        <div className="video-list">
          {showVideos &&
            videos.map((video) => <Video key={video.src} src={video.src} />)}
        </div>
      </div>

      <style jsx>{`
        .cinema-page {
          display: grid;
          grid-template-columns: auto;
          row-gap: 10px;
        }

        .cookie-info {
          background: #fff;
          width: 90vw;
          padding: 5px 10px;
          max-width: 800px;
        }

        .cookie-checkbox {
          margin: 10px 0;
        }

        .okBtn {
          padding: 5px 10px;
          cursor: pointer;
          float: right;
          background: yellow;
        }

        .video-list {
          max-height: 80vh;
          overflow-y: scroll;
          display: grid;
          row-gap: 20px;
        }
      `}</style>
    </Layout>
  );
}
