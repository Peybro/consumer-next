import Head from "next/head";
import Back from "../components/Back";
import Layout from "../layouts/IntroLayout";
import ProductCard from "../components/ProductCard";

export default function Shop() {
  return (
    <Layout>
      <div className="shop-page">
        <Head>
          <title>Shop</title>
          <meta name="description" content="Consume here." />
        </Head>
        <Back to="/intro" />
        <div className="product-list">
          {[
            {
              title: "Limited Edition",
              src:
                "https://static.wixstatic.com/media/bab4d2_c68350e58bb84e648fe6351784e1eb8d~mv2.jpg/v1/fill/w_350,h_350,al_c,q_80,usm_0.66_1.00_0.01/order.webp",
              alt: "Limited Edition",
              link:
                "https://docs.google.com/forms/d/e/1FAIpQLSesKY8MA-IcLugUE1op9gyiocfhmhQpkWtOztR31ko90tV7gA/viewform?usp=sf_link",
            },
            {
              title: "Other Stuff",
              src:
                "https://static.wixstatic.com/media/bab4d2_c9574cb3331e4aaebd5a3cdee2b7dd2e~mv2.jpg/v1/fill/w_345,h_350,al_c,q_80,usm_0.66_1.00_0.01/Ohne%20Titel%20(1).webp",
              alt: "Other Stuff",
              link:
                "https://docs.google.com/forms/d/e/1FAIpQLSesKY8MA-IcLugUE1op9gyiocfhmhQpkWtOztR31ko90tV7gA/viewform?usp=sf_link",
            },
          ].map((product) => {
            return (
              <ProductCard
                key={product.title}
                name={product.title}
                imgSrc={product.src}
                imgAlt={product.alt}
                link={product.link}
              />
            );
          })}
        </div>
      </div>

      <style jsx>{`
        .shop-page {
          display: grid;
          grid-template-columns: auto;
          row-gap: 10px;
        }

        .product-list {
          max-width: 90vw;
          margin-bottom: 20px;
        }

        @media screen and (min-width: 320px) {
          .shop-page {
            overflow: hidden;
          }

          .product-list {
            display: grid;
            row-gap: 10px;
            grid-template-columns: auto;
          }
        }

        @media screen and (max-width: 320px) {
          .shop-page {
            overflow: scroll;
          }

          .product-list {
            display: grid;
            grid-template-columns: auto;
          }
        }

        @media screen and (min-width: 720px) {
          .product-list {
            display: grid;
            grid-column-gap: 20px;
            grid-template-columns: auto auto;
          }
        }
      `}</style>
    </Layout>
  );
}
