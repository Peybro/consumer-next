import Head from "next/head";
import Link from "next/link";
import Consumer from "../components/Consumer";

export default function Home() {
  return (
    <div className="start-page">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="background">
        <Consumer amount={300} />
      </div>
      <div className="header-container">
        <Link href="/intro">
          <a className="header">
            <h1>Consumer</h1>
            <h2>/kənˈsjuːmə(r)/</h2>
          </a>
        </Link>
      </div>

      <style jsx>{`
        .start-page {
          margin: 0;
          padding: 0;

          height: 100vh;
          overflow: hidden;
        }

        .background {
          margin-top: -3%;
          margin-left: -7%;
          width: 140vw;
          color: #fff;
          animation: rainbow 1s infinite;
          background: #000;
          font-size: 4rem;
          position: fixed;
          filter: blur(2px);
          word-break: break-all;
        }

        .header-container {
          display: flex;
          height: 100vh;
          justify-content: center;
          align-items: center;
        }

        .header {
          color: yellow;
          font-size: 2rem;
          z-index: 10;
          text-decoration: none;
          text-align: center;
        }

        @keyframes rainbow {
          0% {
            color: pink;
          }
          25% {
            color: lightgreen;
          }
          75% {
            color: cyan;
          }
          100% {
            color: pink;
          }
        }
      `}</style>
    </div>
  );
}
