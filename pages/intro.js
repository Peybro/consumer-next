import Layout from "../layouts/IntroLayout";
import Card from "./../components/IntroCard";

export default function Intro() {
  return (
    <Layout>
      <div className="card-container">
        <Card
          title={"Shopping"}
          to={"/shop"}
          img={{
            alt: "cart",
            src:
              "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fdijf55il5e0d1.cloudfront.net%2Fimages%2Fna%2F3%2F7%2F4%2F37497_1000.jpg&f=1&nofb=1",
          }}
        />
        <Card
          title={"Cinema"}
          to={"/cinema"}
          img={{
            alt: "cinema",
            src:
              "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fc.pxhere.com%2Fphotos%2Ff5%2Fbe%2Fcolumbus_ohio_ohio_theatre_theater_marquee_front_entrance_downtown-1206124.jpg!d&f=1&nofb=1",
          }}
        />
      </div>

      <style jsx>{`
        .card-container {
          display: grid;
          grid-column-gap: 50px;
          row-gap: 50px;
        }

        @media screen and (max-width: 320px) {
          .card-container {
            grid-template-columns: auto;
            row-gap: 20px;
          }
        }

        @media screen and (min-width: 720px) {
          .card-container {
            grid-template-columns: auto auto;
          }
        }
      `}</style>
    </Layout>
  );
}
