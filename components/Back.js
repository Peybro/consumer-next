import Link from "next/link";

export default function Back({ to }) {
  return (
    <Link href={to}>
      <a className="backBtn">
        {"<-back"}

        <style jsx>{`
          .backBtn {
            font-size: 2rem;
            text-decoration: none;
            color: yellow;
            background: rgb(163, 163, 163);
            padding: 5px 10px;
          }
        `}</style>
      </a>
    </Link>
  );
}
