import Link from "next/link";

export default function Introcard({ title, to, img }) {
  return (
    <Link href={to}>
      <a className="card">
        <img className="card-img" alt={img.alt} src={img.src} />
        <div className="card-body">
          <p>{title}</p>
          <p>{title}</p>
          <p>{title}</p>
          <p>{title}</p>
        </div>

        <style jsx>{`
          .card {
            display: flex;
            max-width: 400px;
            max-height: 400px;
            overflow: hidden;
            position: relative;
            justify-content: center;
            align-items: center;
            background: #fff;
          }

          .card-img {
            width: 80%;
            position: absolute;
            filter: grayscale(1) contrast(0.5);
          }

          .card-body {
            position: absolute;
            color: yellow;
            font-style: italic;
          }

          @media screen and (min-width: 100px) {
            .card {
              width: 80vw;
              height: 80vw;
            }

            .card-body {
              font-size: 3rem;
            }
          }

          @media screen and (min-width: 720px) {
            .card {
              width: 40vw;
              height: 40vw;
            }

            .card-body {
              font-size: 4rem;
            }
          }
        `}</style>
      </a>
    </Link>
  );
}
