import { useEffect, useState } from "react";

export default function Video({ title, src }) {
  const [style, setStyle] = useState({
    width: window.innerWidth * 0.8,
    height: (window.innerWidth * 0.8) / (777 / 436),
  });

  const handleResize = () => {
    setStyle({
      width: window.innerWidth * 0.8,
      height: (window.innerWidth * 0.8) / (777 / 436),
    });
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div className="video-container" style={style}>
      <iframe
        title={title}
        src={src}
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />

      <style jsx>{`
        .video-container {
            max-width: 777px;
            max-height: 436px;
        }
      
        .video-container iframe,
        .video-container object,
        .video-container embed {
            width: 100%;
            height: 100%;
        }
      }
      `}</style>
    </div>
  );
}
