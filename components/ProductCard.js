export default function Product({ title, imgSrc, imgAlt, link }) {
  return (
    <a className="product-card" href={link} target="_blank">
      <img className="product-img" src={imgSrc} alt={imgAlt} />

      <style jsx>{`
        .product-card {
          display: flex;
          max-width: 400px;
          max-height: 400px;
          overflow: hidden;
          position: relative;
          justify-content: center;
          align-items: center;
          background: #fff;
        }

        .product-img {
          width: 100%;
        }
      `}</style>
    </a>
  );
}
